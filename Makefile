IMG:=ertugerata/pisi-base

build:
	docker build -t $(IMG) .
	
	bash build-base.sh

clean:
	docker rmi $(IMG)
