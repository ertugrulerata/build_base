# build_base

works for creating pisi base image

```
git clone https://github.com/ertugerata/build_base.git
docker build -t="pisi-base" build_base/
```

# for test in [PWD][1] 
other platforms that unneeded.

```
apk update && apk add tar
```

# build your pisi_beta image
```
cd build_base && sh build_base.sh
```

# enjoy 

```
docker images
REPOSITORY            TAG                 IMAGE ID            CREATED             SIZE
pisi_beta             latest              a9f7142be092        21 seconds ago      184MB

```

[1]:https://labs.play-with-docker.com/
