#!/bin/bash

alias pisi='docker run --rm -v $(pwd):/root pisi-base'

rm -rf $(pwd)/rootfs

### depo ekle
pisi ar Beta http://ciftlik.pisilinux.org/2.0-Beta.1/pisi-index.xml.xz -D rootfs

### base yükle 
pisi it -D rootfs --ignore-comar --ignore-dep --ignore-safety $(cat ./pisi_list)

### gerekenler

mkdir -p rootfs/root
cp /etc/resolv.conf rootfs/etc/.
cp /etc/localtime rootfs/etc/.
cp rootfs/usr/share/baselayout/* rootfs/etc/.

#### temizlik 
rm -rf rootfs/usr/share/man && \
rm -rf rootfs/usr/share/doc && \
rm -rf rootfs/usr/share/gtk-doc && \
rm -rf rootfs/usr/lib/locale/locale-archive && \
rm -rf rootfs/usr/local && \
rm -rf rootfs/usr/lib/python2.7/test && \
rm -rf rootfs/usr/share/locale/[a-d,f-z]* && \
rm -rf rootfs/usr/share/locale/e[a-m,o-z,@,_]* && \
rm -rf rootfs/var/cache/pisi/packages && \
rm -rf rootfs/var/cache/pisi/archives

#### depo sil
pisi rr Beta -D rootfs

## image oluştur
cd $(pwd)/rootfs && tar --numeric-owner --xattrs --acls -c . | docker import - pisi_beta