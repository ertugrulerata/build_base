#ertugerata/pisi-base
FROM ertugerata/pisi_beta
LABEL maintainer Ertuğrul Erata <ertugrulerata@gmail.com>

RUN service dbus start && pisi cp && service dbus stop

WORKDIR /root

ENTRYPOINT ["pisi"]
